package com.example.message.controller;

import com.example.message.entity.Messages;
import com.example.message.exception.EmptyListOfMessageException;
import com.example.message.service.MessageService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import wiremock.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MessageController.class)
public class MessageControllerTest {

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MessageService messageService;

    @Test
    public void shouldRetrieveListOfMessages() throws Exception {
        List<Messages> expected = new ArrayList<>();
        expected.add(new Messages("0423112421", "test message", "19/10/2021", "ACTIVE"));
        Mockito.when(messageService.getMessage())
                .thenReturn(expected);

        String url = "/getmessage";
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                        .get(url)
                        .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();
        String actualResponse = response.getResponse().getContentAsString();

        String expectedJsonResponse = mapper.writeValueAsString(expected);
        Assertions.assertEquals(actualResponse, expectedJsonResponse);
    }

    @Test
    public void shouldThrowExceptionWhenReturnEmptyListOfMessages() throws Exception {
        Mockito.when(messageService.getMessage())
                .thenThrow(EmptyListOfMessageException.class);
        String url = "/getmessage";
        mockMvc.perform(MockMvcRequestBuilders
                        .get(url)
                        .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldSendNewMessage() throws Exception{
        String url = "/sendmessage";
        Messages newMessage = new Messages("0423112421", "test message", "19/10/2021", "ACTIVE");
        Mockito.when(messageService.sendMessage(any(Messages.class)))
                .thenReturn(new Messages("0423112421", "test message", "19/10/2021", "ACTIVE"));
        mockMvc.perform(MockMvcRequestBuilders
                        .post(url)
                        .content(asJsonString(newMessage))
                        .contentType(APPLICATION_JSON_VALUE)
                        .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.number").value(newMessage.getNumber()));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
