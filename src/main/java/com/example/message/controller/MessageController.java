package com.example.message.controller;

import com.example.message.entity.Messages;
import com.example.message.exception.EmptyListOfMessageException;
import com.example.message.service.MessageService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@CrossOrigin(origins = "*")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @ApiOperation(
            value = "Retrieve message",
            notes = "Retrieve list of message",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Retrieve list of message successfully"),
            @ApiResponse(code = 404, message = "Message not found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, value = "/getmessage")
    @ResponseBody
    public List<Messages> getAllMessage() throws EmptyListOfMessageException {
        return messageService.getMessage();
    }

    @ApiOperation(value = "Create new message", notes = "Create a new message", produces = APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Create new message successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/sendmessage", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Messages sendMessage(
            @ApiParam(required = true)
            @RequestBody Messages messages) {
        return messageService.sendMessage(messages);
    }

}
