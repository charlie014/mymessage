## Build
Build the project
```
mvn clean install
```

## Front-end
Direct to the path: ~message/src/main/frontend/src
```
npm install - install the React front-end
npm start - start the server
```

## Springboot back-end
Back end has already been run on the AWS EC2 server with the url:
```
http://ec2-52-64-75-73.ap-southeast-2.compute.amazonaws.com:8080/getmessage - get all the messages
http://ec2-52-64-75-73.ap-southeast-2.compute.amazonaws.com:8080/sendmessage - post new message
```

I was trying to run them continuously by adding the "&" at the end to the run job in AWS EC2 however, I couldn't figured
it out how to run them live after quit the terminal. Here is some evidence that I have run them live.

![](../../../../../Desktop/AWS_1.png)![](../../../../../Desktop/AWS_2.png)![](../../../../../Desktop/AWS_3.png)

If there is any questions regarding the deployment, I can answer and explain the process. 
Appreciate!
