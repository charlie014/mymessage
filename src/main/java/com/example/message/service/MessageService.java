package com.example.message.service;

import com.example.message.entity.Messages;
import com.example.message.exception.EmptyListOfMessageException;
import com.example.message.repository.MessageRepository;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messagesRepository;

    @Transactional
    public Messages sendMessage(@NotNull Messages newMessage) {
        return messagesRepository.save(newMessage);
    }

    @Transactional
    public List<Messages> getMessage() throws EmptyListOfMessageException {
        List<Messages> response = messagesRepository.findAll();

        if (response.isEmpty()) {
            throw new EmptyListOfMessageException("Empty List Of Message");
        } else {
            return response;
        }
    }
}
