import './App.css';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import React, {useState} from "react";
import Button from '@mui/material/Button';



function App() {
  const [value, setValue] = useState({
    destinationNumber: "",
    message: ""
  });

    const [appState, setAppState] = useState({
        repos: [],
    });

  const handleChange = (prop) => (event) => {
    setValue({ ...value, [prop]: event.target.value });
  };

  async function sendMessage() {
    if (value.destinationNumber && value.message) {
        let time = Date().toLocaleString()
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                number: value.destinationNumber,
                message: value.message,
                timestamp: time.toString(),
                status: "SENT"})
        };

        const apiUrlPost = `http://ec2-52-64-75-73.ap-southeast-2.compute.amazonaws.com:8080/sendmessage`;
        fetch(apiUrlPost, requestOptions)
            .then((res) => res.json())
            .then((data) => {
                console.log(data)
            })
            .catch(err => {
                console.log(err)
            })
    }
  }

  async function retrieveMessage() {
      const apiUrl = `http://ec2-52-64-75-73.ap-southeast-2.compute.amazonaws.com:8080/getmessage`;
      fetch(apiUrl)
          .then((res) => res.json())
          .then((repos) => {
              setAppState({ repos: repos });
      });
  }

  return (
    <div className="App">
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="right">Id</TableCell>
              <TableCell align="right">Number</TableCell>
              <TableCell align="right">Message</TableCell>
              <TableCell align="right">Timestamp</TableCell>
              <TableCell align="right">Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {appState.repos && appState.repos.map((row) => (
                <TableRow
                    key={row.name}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell align="right">{row.id}</TableCell>
                  <TableCell align="right">{row.number}</TableCell>
                  <TableCell align="right">{row.message}</TableCell>
                  <TableCell align="right">{row.timestamp}</TableCell>
                  <TableCell align="right">{row.status}</TableCell>
                </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <br/>
      <br/>
      <TextField
          id="outlined-password-input"
          label="Destination Number"
          type="text"
          autoComplete="destination-number"
          style={{marginRight: 20}}
          value={value.destinationNumber}
          onChange={handleChange('destinationNumber')}
      />
      <TextField
          id="outlined-multiline-static"
          label="Message"
          multiline
          rows={4}
          placeholder = "Please type your message here"
          style={{width: 200, marginRight: 40}}
          value={value.message}
          onChange={handleChange('message')}
      />

      <Button variant="contained" onClick ={() => sendMessage()}>
        Send
      </Button>
      <Button variant="contained" style={{marginLeft: 50}} onClick={() => retrieveMessage()}>
        Show all messages
      </Button>
      <br/>
    </div>
  );
}

export default App;
