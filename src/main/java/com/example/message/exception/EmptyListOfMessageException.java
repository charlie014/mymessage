package com.example.message.exception;

import javassist.NotFoundException;

public class EmptyListOfMessageException extends NotFoundException {

    public EmptyListOfMessageException(String msg) {
        super(msg);
    }
}
