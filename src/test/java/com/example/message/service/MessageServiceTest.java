package com.example.message.service;

import com.example.message.repository.MessageRepository;
import com.example.message.entity.Messages;
import com.example.message.exception.EmptyListOfMessageException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class MessageServiceTest {
    @Mock
    private MessageRepository messagesRepository;

    @InjectMocks
    private MessageService messageService;

    @Test
    public void shouldGetAllMessages() throws EmptyListOfMessageException {
        List<Messages> expectedListOfMessages = new ArrayList<>();
        expectedListOfMessages.add(new Messages(
                "0423112421", "test message", "19/10/2021", "ACTIVE"));
        Mockito.when(messagesRepository.findAll())
                .thenReturn(expectedListOfMessages);

        List<Messages> actual = messageService.getMessage();
        Assertions.assertEquals(expectedListOfMessages, actual);
    }

    @Test
    public void shouldThrowExceptionWhenReturnEmptyListOfMessage() {
        Mockito.when(messagesRepository.findAll())
                .thenReturn(new ArrayList<>());
        Exception thrown = Assertions.assertThrows(EmptyListOfMessageException.class, () ->
                messageService.getMessage()
        );
        Assertions.assertTrue(thrown.getMessage().contains("Empty List Of Message"));
    }

    @Test
    public void shouldMessageNewMessage() {
        Messages newMessage = new Messages(
                "0423112421", "test message", "19/10/2021", "ACTIVE");
        Mockito.when(messagesRepository.save(newMessage))
                .thenReturn(new Messages(
                        "0423112421", "test message", "19/10/2021", "ACTIVE"
                ));
        Messages expected = messageService.sendMessage(newMessage);
        Assertions.assertEquals(expected.getNumber(), "0423112421");
    }
}
