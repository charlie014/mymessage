package com.example.message.exception;

import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {EmptyListOfMessageException.class})
    public ResponseEntity<ErrorException> handleNotFoundException(NotFoundException e) {

        ErrorException errorException = new ErrorException("NOT_FOUND_ERROR", e.getMessage());
        errorException.setTimestamp(LocalDateTime.now());
        errorException.setStatus(HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(errorException, HttpStatus.NOT_FOUND);
    }
}
